
#include "stdafx.h"
#include <Windows.h>
#include "list"
#include <iostream>

DWORD WINAPI ThreadFunc(LPVOID lpdwParam) {
	Sleep(INFINITE);
	return 0;
}

int _tmain(int argc, _TCHAR* argv[])
{
	DWORD dwThreadId;
	std::list<HANDLE> lista;
	HANDLE ahThread;
	volatile unsigned long long int i = 0;
	char* _emergencyMemory = new char[16384];
	try {
		while (true)
		{
			ahThread = CreateThread(NULL, 0, ThreadFunc, (PVOID)i, 0, &dwThreadId);
			lista.push_back(ahThread);
			printf_s("Watek numer: %d\n", i);
			i++;
		}
	}
	catch (std::bad_alloc ex) {
		// Delete the reserved memory so we can print an error message before exiting
		delete[] _emergencyMemory;
		lista.clear();
		CloseHandle(ahThread);
		std::cerr << "Maks. liczba watkow: " << i << " - Out of memory!\n";
		system("pause");
	}
	return 0;
}
